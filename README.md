# Daemonium Experiment

# Sumário


* [Introdução](#Introdução)
* [Pré-Requisitos](#Pré-Requisitos)
* [Documentação](#Documentação)

# Introdução

Daemonium Experiment é um software desenvolvido utilizando a Unity Engine
a qual facilita o desenvolvimento de jogos. Daemonium Experiment será um jogo
em primeira pessoa no estilo de survival horror.
O projeto teve inicio na cadeira de Projeto de Desenvolvimento da Faculdade de Tecnologia Senac Pelotas.
a qual conta com a orientação do professor Angelo Luz. 
O projeto ao final deverá fornecer uma demo atendendo todas as funcionalidades
propostas durante o desenvolvimento.

🚧 Situação atual do projeto: em construção.
Sinta-se livre para baixar o código-fonte do projeto.

# Pré-Requisitos

A Unity assim como outras ferramentas necessitam de alguns requisitos de sistema.
O que varia muito dependendo da complexidade do projeto.
Para mais informações acessar o site.
Importante: A Unity está disponível para Mac OS X e Windows e algumas versões para Linux.
O projeto está sendo desenvolvido com a versão grátis da Unity mas conta com a compra de assets exclusivos.
O jogo começou a ser desenvolvido na versão 2018.3.10 e atualmente se encontra 
utilizando a versão 2019.2.1f1

 ⚠ ATENÇÃO, as ferramentas, comandos, linguagens requerem conhecimentos prévios!
 
 * [Requisitos de Sistema Unity](https://unity3d.com/pt/unity/system-requirements)
 * [Site Oficial Unity](https://unity.com/pt)
 * [Download Unity](https://store.unity.com/?_ga=2.126865934.249640677.1553827386-1206357891.1546796747)
 
# Documentação

A documentação se encontra na [Wiki](https://gitlab.com/senac-tcc-2019/sergio-renato-dias-filho/wikis/home) do projeto
